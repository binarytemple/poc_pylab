#!/usr/bin/python

#Python - Tidy and plot

from matplotlib.pylab import plt
from datetime import datetime

def hour_n_day(timestamp):
    t_tup =datetime.fromtimestamp(int(timestamp)).timetuple()
    return (t_tup.tm_yday,t_tup.tm_hour)


fh=open('/tmp/graph.txt','ra')

lines=fh.read().splitlines()

#Get a dayofyear, hourofday tuple list
lf=map(lambda x: hour_n_day(float(x)) , lines)

#Translate that into two arrays..
two_arrays=reduce(lambda x,y: ( ( x[0] + [y[0]]  ,x[1] + [y[1]] )   ), lf, ([1],[2]) )

plt.scatter(two_arrays[0],two_arrays[1])
plt.show() 
